<?php

  get_header();

  while ( have_posts() ) : the_post();

  $thumbnail = getPostImage(get_the_ID(), 'full');
  $category = getPostDisplayCategory(get_the_ID());
  $author = get_the_author();
  $date = get_the_time('jS F Y');
 	$mainPostId = get_the_ID();

 ?>

<main class="site-content">

    <section class="hero-post align--center">

          <div class="post post--large">
            <div class="post__bannerad">
                <br />
              <div id="RTK_0yYk" style="overflow:hidden"></div>
              <br />
            </div>
          </div>

        </section>

        <section class="columned-content">

          <aside class="column-left align--center">
            <div id="RTK_qw1q" style="overflow:hidden"></div>

            <?php

              if(!$_GET['lazy']){

            ?>
            <section class="related-stories align--center">

              <div class="container">

                <h2 class="page-title font--16px">Related Stories</h2>
                <div class="divider"></div>

                <?php

                $sticky = get_option( 'sticky_posts' );
                $relatedArgs = array(
                  'posts_per_page'=>4,
                  'orderby'=>'date',
                  'post__not_in'=>array_merge(array($mainPostId), $sticky),
                  'category_name'=>$category
                );

                $relatedQuery = new WP_Query($relatedArgs);

                while($relatedQuery->have_posts()) :

                  $relatedQuery->the_post();

                  $thumbnail = getPostImage(get_the_ID(), 'listing-thumb');
                  $category = getPostDisplayCategory(get_the_ID());
                  $author = get_the_author();
                  $date = get_the_time('jS F Y');

                  echo '<a class="post post--small post--sidebar col" href="' . get_permalink() . '">
                      <div class="post__image-container"><div class="post__image z--0" style="background-image:url(' . $thumbnail . ')"></div>
                        <div class="pre-title pre-title--tag extra font--white font--12px">' . $category . '</div>
                      </div>
                      <div class="post__info">
                        <h1 class="news-title font--16px">' . get_the_title() . '</h1>
                        <div class="divider"></div>
                        <p class="post-data font--12px">By ' . $author . '</p>
                        <p class="post-data font--12px">' . $date . '</p>
                      </div>
                    </a>';

                endwhile;

                wp_reset_query();

                ?>

              </div>

            </section>
            <?php
              }
            ?>

		&nbsp;
          </aside>

          <section class="article-content article-content--open align--center">

            <div class="container container--tight">

              <section class="hero-post align--center">

                      <div class="post post--inline">

                        <div class="post__info post__info--no-margin">
                          <div class="pre-title pre-title--tag font--white font--12px"><?php echo $category ?></div>
                          <h1 class="article-title news-title font--40px"><?php echo get_the_title(); ?></h1>
                          <div class="divider"></div>
                          <p class="post-data font--16px">By <?php echo $author ?></p>
                          <p class="post-data font--16px"><?php echo $date ?></p>
                          <div class="divider"></div>
                          <div class="share-buttons">
                            <p class="share">Share this article</p>

                            <?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>

                          </div>
                        </div>
                      </div>

                    </section>

  			  <pb-mcd embed-id="32ab8702-9a39-4ccd-9046-4c9031f5029a"></pb-mcd>

  <script>

  (function(){

     var player_id = "";
     var player_name = "";

  try{

  function getUrlParameter(name) {
     name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
     var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
     var results = regex.exec(location.search);
     return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
  };


  var player_obj = {
     "default":{
         "id":"32ab8702-9a39-4ccd-9046-4c9031f5029a",
         "name":"mcd-sdk-jssdk_dailyfeed_player"
     },
     "fbdfpaid": {
         "id":"d2888e39-ef70-4e63-a7de-5930bf6bb3f3",
         "name":"mcd-sdk-jssdk_dailyfeed_Player_FBDFPaid"
     },
     "fbaapaid": {
         "id":"a2265ee6-991f-413b-9105-20a98f654a6c",
         "name":"mcd-sdk-jssdk_dailyfeed_Player_FBAAPaid"
     },
     "scpaid":{
         "id":"7b2bbe95-4e7c-49bd-828b-4cf7a0e11163",
         "name":"mcd-sdk-jssdk_dailyfeed_Player_SCPaid"
     },
     "vt":{
         "id":"79232d50-9b4b-4156-9c66-b3601b0d98a2",
         "name":"mcd-sdk-jssdk_dailyfeed_Player_VT"
     }
  };

     var utm_source = getUrlParameter('utm_source');

     if(typeof utm_source === 'string'){
         utm_source = utm_source.toLowerCase();
     }

     if( utm_source && player_obj[utm_source] ){

         player_id = player_obj[utm_source].id;
         player_name = player_obj[utm_source].name;

     }else{
         player_id = player_obj.default.id;
         player_name = player_obj.default.name;
     }

     document.querySelector('pb-mcd[embed-id="32ab8702-9a39-4ccd-9046-4c9031f5029a"]').setAttribute("embed-id",player_id);



  }catch(e){
      player_id = "32ab8702-9a39-4ccd-9046-4c9031f5029a";
      player_name = "mcd-sdk-jssdk_dailyfeed_player";
  }

   (function (d,s,n,id) {
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) return;
     js = d.createElement(s);
     js.id = id;
     js.className = n;
     js.src = "https://mcd-sdk.playbuzz.com/embed/sdk.js?embedId=" + player_id;
     fjs.parentNode.insertBefore(js, fjs);
   }( document,'script','playbuzz-mcd-sdk', player_name ));

  })();




  </script>




              <?php echo the_content(); ?>
              
              <?php

                $nextPost = get_previous_post(true);
                echo daily_feed_post_pages($nextPost);
              ?>
	<div id="RTK_Is40" style="overflow:hidden"></div>
              <div class="share-buttons">
              <span class="share">Share this article</span>
              <?php if ( function_exists( 'ADDTOANY_SHARE_SAVE_KIT' ) ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>
              <br /><br />
            </div>
          </section>

      <?php

      endwhile; // End of the loop.

      ?>

      <aside class="column-right align--center">
          <div id="RTK_eOSi" style="overflow:hidden"></div>
        <?php

            if(!$_GET['lazy']){

          ?>
        <section class="related-stories align--center">

          <div class="container">

            <h2 class="page-title font--16px">Related Stories</h2>
            <div class="divider"></div>

            <?php
            $sticky = get_option( 'sticky_posts' );
            $relatedArgs = array(
              'posts_per_page'=>4,
              'offset'=>4,
              'orderby'=>'date',
              'post__not_in'=>array_merge(array($mainPostId), $sticky),
              'category_name'=>$category
            );

            $relatedQuery = new WP_Query($relatedArgs);

            while($relatedQuery->have_posts()) :

              $relatedQuery->the_post();

              $thumbnail = getPostImage(get_the_ID(), 'listing-thumb');
              $category = getPostDisplayCategory(get_the_ID());
              $author = get_the_author();
              $date = get_the_time('jS F Y');

              echo '<a class="post post--small post--sidebar col" href="' . get_permalink() . '">
                  <div class="post__image-container"><div class="post__image z--0" style="background-image:url(' . $thumbnail . ')"></div>
                    <div class="pre-title pre-title--tag extra font--white font--12px">' . $category . '</div>
                  </div>
                  <div class="post__info">
                    <h1 class="news-title font--16px">' . get_the_title() . '</h1>
                    <div class="divider"></div>
                    <p class="post-data font--12px">By ' . $author . '</p>
                    <p class="post-data font--12px">' . $date . '</p>
                  </div>
                </a>';

            endwhile;

            wp_reset_query();

            ?>

          </div>

        </section>

        <?php

          }

        ?>
		&nbsp;
      </aside>

    </section>



<br clear="all" />

</main>

 <?php



 get_footer();

  ?>
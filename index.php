<?php

get_header();

$sticky = get_option( 'sticky_posts' );

krsort($sticky);
$sticky = array_values($sticky);

?>

  <main class="site-content">

    <section class="hero-post align--center">

        <?php

          $featuredArgs = array(
            'posts_per_page'=>1,
            'orderby'=>'date',
            'p'=>$sticky[0],
            'ignore_sticky_posts' => 1
          );

          $featuredQuery = new WP_Query($featuredArgs);

          while($featuredQuery->have_posts()) :

            $featuredQuery->the_post();

            $thumbnail = getPostImage(get_the_ID(), 'large');
            $category = getPostDisplayCategory(get_the_ID());
            $author = get_the_author();
            $date = get_the_time('jS F Y');

            echo '<a class="post post--large" href="' . get_permalink() . '">
              <div class="post__image-container"><div class="post__image z--0" style="background-image:url(' . $thumbnail . ')"></div></div>
              <div class="post__info">
                <div class="pre-title pre-title--tag font--white font--12px">' . $category . '</div>
                <h1 class="news-title font--40px">' . get_the_title() . '</h1>
                <div class="divider"></div>
                <p class="post-data font--16px">By ' . $author .'</p>
                <p class="post-data font--16px">' . $date .'</p>
              </div>
            </a>';


          endwhile;

          wp_reset_query();

         ?>

      </section>

      <?php

      $editorPickArgs = array(
        'posts_per_page'=>4,
        'orderby'=>'date',
        'tag'=>'topstory',
        'ignore_sticky_posts' => 1
      );

      $editorPicksQuery = new WP_Query($editorPickArgs);

      if($editorPicksQuery->have_posts()){

        echo '<section class="editors-pick align--center">

       <div class="columns columns--4 container">

         <h2 class="page-title font--22px">Top Stories</h2>
         <div class="divider"></div>';

        while($editorPicksQuery->have_posts()):

          $editorPicksQuery->the_post();

          $thumbnail = getPostImage(get_the_ID(), 'carousel');
          $category = getPostDisplayCategory(get_the_ID());
          $author = get_the_author();
          $date = get_the_time('jS F Y');

          echo '<a class="post post--small col" href="' . get_permalink() . '">
           <div class="post__image-container"><div class="post__image z--0" style="background-image:url(' . $thumbnail . ')"></div>
             <div class="pre-title pre-title--tag extra font--white font--12px">' . $category . '</div>
           </div>
           <div class="post__info">
             <h1 class="news-title font--22px">' . get_the_title() . '</h1>
             <div class="divider"></div>
             <p class="post-data font--16px">By ' . $author .'</p>
             <p class="post-data font--16px">' . $date .'</p>
           </div>
         </a>';


        endwhile;

          echo '</div>
       </section>';

       }

      wp_reset_query();

   ?>

      <section class="align--center">

      <?php

        $featuredArgs = array(
          'posts_per_page'=>1,
          'orderby'=>'date',
          'p'=>$sticky[1],
          'ignore_sticky_posts' => 1
        );

        $featuredQuery = new WP_Query($featuredArgs);

        while($featuredQuery->have_posts()) :

          $featuredQuery->the_post();

          $thumbnail = getPostImage(get_the_ID(), 'large');
          $category = getPostDisplayCategory(get_the_ID());
          $author = get_the_author();
          $date = get_the_time('jS F Y');

          echo '<a class="post post--large" href="' . get_permalink() . '">
            <div class="post__image-container"><div class="post__image z--0" style="background-image:url(' . $thumbnail . ')"></div></div>
            <div class="post__info">
              <div class="pre-title pre-title--tag font--white font--12px">' . $category . '</div>
              <h1 class="news-title font--40px">' . get_the_title() . '</h1>
              <div class="divider"></div>
              <p class="post-data font--16px">By ' . $author .'</p>
              <p class="post-data font--16px">' . $date .'</p>
            </div>
          </a>';


        endwhile;

        wp_reset_query();

       ?>

     </section>



       <?php

       $editorPickArgs = array(
         'posts_per_page'=>4,
         'orderby'=>'date',
         'tag'=>'editorpick',
         'ignore_sticky_posts' => 1
       );

       $editorPicksQuery = new WP_Query($editorPickArgs);

       if($editorPicksQuery->have_posts()){

         echo '<section class="editors-pick align--center">

        <div class="columns columns--4 container">

          <h2 class="page-title font--22px">Editors Pick</h2>
          <div class="divider"></div>';

         while($editorPicksQuery->have_posts()):

           $editorPicksQuery->the_post();

           $thumbnail = getPostImage(get_the_ID(), 'carousel');
           $category = getPostDisplayCategory(get_the_ID());
           $author = get_the_author();
           $date = get_the_time('jS F Y');

           echo '<a class="post post--small col" href="' . get_permalink() . '">
            <div class="post__image-container"><div class="post__image z--0" style="background-image:url(' . $thumbnail . ')"></div>
              <div class="pre-title pre-title--tag extra font--white font--12px">' . $category . '</div>
            </div>
            <div class="post__info">
              <h1 class="news-title font--22px">' . get_the_title() . '</h1>
              <div class="divider"></div>
              <p class="post-data font--16px">By ' . $author .'</p>
              <p class="post-data font--16px">' . $date .'</p>
            </div>
          </a>';


         endwhile;

           echo '</div>
        </section>';

        }

       wp_reset_query();

    ?>

    <section class="align--center">

      <div class="columns columns--2 container">

    <?php

      $featuredArgs = array(
        'posts_per_page'=>2,
        'orderby'=>'date',
        'ignore_sticky_posts' => 1
      );

      $featuredQuery = new WP_Query($featuredArgs);

      while($featuredQuery->have_posts()) :

        $featuredQuery->the_post();

        $thumbnail = getPostImage(get_the_ID(), 'carousel');
        $category = getPostDisplayCategory(get_the_ID());
        $author = get_the_author();
        $date = get_the_time('jS F Y');

        echo '<a class="post post--med col" href="' . get_permalink() . '">
            <div class="post__image-container"><div class="post__image z--0" style="background-image:url(' . $thumbnail . ')"></div>
              <div class="pre-title pre-title--tag extra font--white font--12px">' . $category . '</div>
            </div>
            <div class="post__info">
              <h1 class="news-title font--22px">' . get_the_title() . '</h1>
              <div class="divider"></div>
              <p class="post-data font--16px">By ' . $author .'</p>
              <p class="post-data font--16px">' . $date .'</p>
            </div>
          </a>';



      endwhile;

      wp_reset_query();

     ?>

    </div>

   </section>

   <section class="featured align--center">

        <div class="container">

          <?php

            $featuredArgs = array(
              'posts_per_page'=>1,
              'orderby'=>'date',
              'p'=>$sticky[2],
              'ignore_sticky_posts' => 1
            );

            $featuredQuery = new WP_Query($featuredArgs);

            while($featuredQuery->have_posts()) :

              $featuredQuery->the_post();

              $thumbnail = getPostImage(get_the_ID(), 'carousel');
              $categories = getPostDisplayCategory(get_the_ID());
              $author = get_the_author();
              $date = get_the_time('jS F Y');

              echo '<a class="post post--feat" href="' . get_permalink() . '">
                <div class="post__image-container"><div class="post__image" style="background-image:url(' . $thumbnail . ')"></div></div>
                <div class="post__info">
                  <p class="pre-title pre-title--tag extra font--white font--12px">' . $category . '</p>
                  <h1 class="news-title font--40px">' . get_the_title() . '</h1>
                  <div class="divider"></div>
                  <p class="post-data font--16px">By ' . $author .'</p>
                  <p class="post-data font--16px">' . $date .'</p>
                </div>
              </a>';

            endwhile;

            wp_reset_query();

           ?>



        </div>
      </section>

      <div class="more-articles align--center">
          <div class="columns columns--4 container">
          <?php
            //echo do_shortcode('[ajax_load_more id="8690127325" container_type="a" css_classes="" ignore_sticky_posts="1" post_type="post" posts_per_page="12" offset="2" images_loaded="true" scroll="false" button_label="Load More" button_loading_label="Loading Posts..."]');
          ?>
          </div>
      </div>

  </main>


<?php

get_footer();

?>

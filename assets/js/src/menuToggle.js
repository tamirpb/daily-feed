function menuToggle() {

  $('.site-header__menu').on('click', function(e) {
    e.preventDefault();
    $(this).toggleClass('open');
    $('.site-header__nav').toggleClass('open');
  });

  $('.site-header__link').on('click', function() {
    $('.site-header__nav, .site-header__menu').removeClass('open');
  });

  if ($(window).width() <= 550) {
    $('.subscribe').html("Subscribe to newsletter!");
  }

}
menuToggle();

<?php

  get_header();

 ?>

 <main class="site-content">

   <section class="align--center page-intro">



             <div class="post__info">

               <h1 class="article-title news-title font--40px">404 Error</h1>
               <div class="divider"></div>



             </div>


         </section>

         <section class="article-content align--center">
           <div class="container">

             <p>Unfortunately you've stumbled across a page that no longer exists. Please return to the <a href="/">homepage</a> or try our search.</p>


         </section>


  </main>

<?php

  get_footer();

 ?>
